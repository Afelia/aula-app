export default {
  Login: {
    'welcome': 'Willkommen bei Aula',
    'login': 'Benutzername',
    'password': 'Passwort',
    'password.reminder': 'Wenn Du eine email-Adresse eingegeben hast, kannst du dein Passwort hier neu setzen.',
    'password.reminder.noemail': 'Solltest du dein Passwort nicht mehr kennen und keine email-Adresse haben, melde dich bitte bei den Admins euer Schule.'
  },
  Footer: {
    'user.terms': 'Nutzungsbedingungen',
    'impressum': 'Impressum'
  },
  Menu: {
    'profileView': 'Profil anzeigen',
    'settings': 'Einstellungen',
    'administration': 'Prozessverwaltung',
    'logout': 'Logout'
  },
  Header: {
    'start': 'Start',
    'delegation': 'Beauftragungen'
  },
  Delegation: {
    'delegation.network': 'Beauftragungsnetzwerk',
    'selected': 'Ausgewählt',
    'all.delegation': 'Delegation für alle Themen',
    'minimum.delegation.number': 'Untergrenze Anzahl Beauftragungen',
    'user.search': 'Nutzer suchen'
  },
  Space: {
    'wildIdeas': 'Wilde Ideen',
    'ideaTopics': 'Themen auf dem Tisch',
    'wild.ideas.title': 'Wilde Ideen',
    'ask.ideas': 'Was soll sich verändern?',
    'ask.ideas.description': 'Du kannst hier jede lose Idee, die du im Kopf hast, einwerfen und kannst für die Idee abstimmen und diese somit "auf den Tisch bringen".',
    'newIdea': 'Neue Idee',
    'idea.filters': 'Filtere nach Kategorie',
    'all.ideas': 'Alle Kategorien',
    'search.idea': 'Suchen',
    'sort.ideas': 'Sortieren nach',
    'idea.supporters': 'Unterstützung',
    'idea.date': 'Datum',
    'no.ideas.search.results': 'Keine Ideen.',
    'ask.on.no.ideas.search.results': 'Erstelle Deine eigene Idee!'
  },
  Topic: {
    'introTitle': 'Diese Themen sind in der Diskussion',
    'introDescription': 'Hinter jedem Thema können mehrere  passende Ideen stehen, die hier vor der Abstimmung diskutiert werden',
    'new.topic': 'Neues Thema',
    'view.theme': 'Thema anzeigen',
    'topic.options': 'Optionen',
    'edit.topic': 'Thema bearbeiten',
    'delete.topic': 'Thema Löschen',
    'advance.topic.phase': 'Nächste Phase',
    'back.topic.phase': 'Vorherige Phase',
    'to.topic.list': 'Zu Allen Themen',
    'tab.topic.ideas': 'Alle Ideen',
    'tab.approved.ideas': 'Angenommene Ideen',
    'tab.winner.ideas': 'Gewinner',
    'tab.delegates': 'Beauftrage Stimmen',
    'back.phase.limit': "Thema {0} konnte nicht zurückgesetzt werden: steht schon auf 'Ausarbeitung'",
    'phase.change': 'Thema {0} wurde von {1} nach {2} verschoben.',
    'phase.time': '{0} (Endet morgen am {1} um ca. {2} Uhr)',
    'your.delegate': 'Derzeit stimmt für dich ab: {0}'
  },
  TopicPhase: {
    'edit_topics': 'Ausarbeitungphase',
    'feasibility': 'Prüfungsphase',
    'vote': 'Abstimmungsphase',
    'finished': 'Ergebnisphase'
  },
  TopicCreation: {
    'topic.creation': 'Thema erstellen',
    'topic.name': 'Wie soll der Titel des Themas lauten?',
    'topic.name.example': 'z.B. Computerraum',
    'topic.description': 'Beschreiben Sie das Thema',
    'topic.description.details': 'Was haben die Ideen dieses Themas gemeinsam?',
    'topic.select.ideas': 'Fügen Sie weitere wilde Ideen dem neuen Thema hinzu',
    'topic.publish': 'Veröffentlichen',
    'cancel': 'Abbrechen',
    'topic.name.empty': 'Title des Themas: ungültige Eingabe: zu wenig Input (erwartet: nicht leer)',
    'topic.description.empty': 'Thema: darf nicht leer sein'
  },
  IdeaCreation: {
    'yourIdea': 'Deine Idee',
    'name': 'Wie soll deine Idee heißen?',
    'nameExample': 'z.B. bessere Ausstatung im Computerraum',
    'suggestion': 'Was möchtest du vorschlagen?',
    'suggestion.description': 'Hier kannst du deine Idee so ausführlich wie möglich beschreiben...',
    'previewShow': 'Vorschau einblenden',
    'selectCategory': 'Kann deine Idee einer der folgenden Kategorieren zugeordnet werden?',
    'publish': 'Idee veröffentlichen',
    'yourDelegates': 'Derzeit Stimmt für dich ab:'
  },
  UserProfile: {
    'delegate.your.vote': 'Für {0} Schule beauftragen',
    'delegates': 'Deine Beauftragung für Schule',
    'remove.delegation': 'Beauftragung für {0} entziehen',
    'report': 'Melden',
    'edit': 'Profil bearbeiten',
    'tab.supported.ideas': 'Erstellte Ideen',
    'tab.delegate.for': 'Für wen stimme ich ab?',
    'tab.my.delegate': 'Wer stimmt für mich ab?',
    'report.descripton': 'Hier kannst du ein Nutzerprofil wegen eines verletzenden oder anstößigen Inhalts beim Moderationsteam melden. Das Team erhält eine Benachrichtigung und wird die Idee schnellstmöglich überprüfen. Bitte gib unten einen Grund an, warum du den Inhalt für anstößig oder verletzend hältst.',
    'report.title': 'Report',
    'report.question': 'Warum möchtest du das Nutzerprofil melden?',
    'report.send': 'Abschicken',
    'delegate.idea': 'Geltungsbereich:',
    'delegates.singular': '{0} Stimme von {1}',
    'delegates.plural': '{0} Stimmen von {1}'
  },
  UserSettings: {
    'email': 'E-mailadresse (optional)',
    'change.password': 'Passwort ändern',
    'current.password': 'aktuelles Passwort',
    'new.password': 'neues Passwort',
    'new.password.confirm': 'neues Passwort bestätigen',
    'save': 'Änderungen speichern'
  },
  IdeaCategories: {
    'rules': 'Regeln',
    'material': 'Ausstatung',
    'activities': 'Aktivitatem',
    'lesson': 'Unterricht',
    'time': 'Zeit',
    'environment': 'Umgebung'
  },
  Idea: {
    'from': 'von {0}',
    'votes.pro': '{0} Quorum-Stimmen',
    'suggestions': '{0} Verbesserungsvorschläge',
    'suggestion': 'Verbesserungsvorschlag',
    'supporters.numbers': '{0} von {1} Quorum-Stimmen',
    'create.topic': 'Thema anlegen',
    'on.table': 'Auf den Tisch!',
    'not.on.table': 'Docht nicht auf den Tisch?',
    'is.possible': 'Durchführbar',
    'not.possible': 'Nicht durchführbar',
    'statement': 'Statement abgeben',
    'categories': 'Diese Idee gehört zur Kategorie',
    'without.category': 'Diese Idee gehört zu keiner Kategorie',
    'new.suggestion': 'Neuer Verbesserungsvorschlag',
    'suggestion.author': '{0} am {1}',
    'suggestion.answer': 'Antworten',
    'suggestion.report': 'Melden',
    'suggestion.edit': 'Bearbeiten',
    'suggestion.delete': 'Löschen',
    'move': 'Idee verschieben',
    'back.to.topic': 'Zum Ideenraum',
    'ask.first.suggestion': 'Gib den ersten Verbesserungsvorschlag ab!',
    'mark.winner': 'Als "gewonnen" markieren',
    'not.winner': 'nicht gewonnen'
  },
  IdeaMove: {
    'move.idea.title': 'Idee verschieben',
    'move.question': "Soll die Idee '{0}' aus '{1}' verschoben werden?",
    'move.to.wild.ideas': "Nach 'wilde Ideen'",
    'move.confirm': 'Verschieben'
  },
  AdminMenu: {
    'title': 'Prozessverwaltung',
    'phaseDuration': 'Dauer der Phasen',
    'quorum': 'Quorum',
    'holidays': 'Ferienmodus',
    'users': 'Gruppen & Nutzer',
    'protocol': 'Protokolle',
    'changePhase': 'Phasen verschieben',
    'termsOfUse': 'Nutzungsbedingungen ändern'
  },
  AdminPhaseTime: {
    'phaseWorking': 'Wie viele Tage soll die Ausarbeitungphase dauern?',
    'phaseVoting': 'Wie viele Tage soll die Abstimmungphase dauren?',
    'days': 'Tage'
  },
  AdminQuorum: {
    'school': 'Wie hoch soll das Quorum schulweit sein?',
    'class': 'Wie hoch soll das Quorum klassenweit sein?',
    'schoolPercent': '% aller Schülerinnen der Schule',
    'classPercent': '% aller Schülerinnen der Klasse'
  },
  AdminHolidays: {
    'description': 'Im Ferienbetrieb sind die folgenden Änderungen zu beachten:',
    'description.1': 'Die Zeit bis zum Ablauf von Ausarbeitungsphase und Abstimmungsphase wird angehalten.',
    'description.2': 'In der wilde-Ideen-Phase kann nicht mehr gewählt werden.',
    'description.3': 'In der Abstimmugnsphase kann nicht mehr gewählt werden.',
    'description.4': 'Es kann nicht mehr auf Kommentare abgestimmt werden.',
    'current.status': 'Aktueller Status',
    'normal.status': 'Normalbetrieb (aufgetaut)',
    'holiday.status': 'Ferienbetrieb (eingefroren)',
    'change.status': 'Status setzen!'
  },
  AdminUsers: {
    'table.name': 'Name',
    'table.class': 'Klass',
    'table.role': 'Role',
    'table.deleted': 'gelöscht',
    'load.users': 'Nutzer anlegen',
    'search.user': 'Nutzersuche',
    'load.class': 'Klasse anlegen',
    'search.class': 'Klassensuche'
  },
  AdminChangePhase: {
    'title': 'Phasen verschieben',
    'description.1': 'ACHTUNG! GEFAHR!',
    'description.2': 'Diese Seite erlaubt es, Themen in beide Richtungen (Zukunft und Vergangenheit) zu verschieben.',
    'description.3': 'Dies ist ein experimentelles Feature, und kann zu unerwartetem Verhalten führen. Bitte nur mit',
    'description.4': 'gutem Grund und nur nach Rücksprache mit den zuständigen Moderatoren durchführen!'
  },
  AdminTermsOfUse: {
    'description': 'Bitte passen Sie hier die Nutzungsbedingungen an'
  }
}
