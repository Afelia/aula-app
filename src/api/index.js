import service from './service'
import { getSchools } from './school'

export default { service, getSchools }
